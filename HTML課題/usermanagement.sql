﻿
CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
CREATE TABLE user(
 id SERIAL PRIMARY KEY UNIQUE Not Null AUTO_INCREMENT,
 login_id varchar(255) UNIQUE Not Null,
 name varchar(255) Not Null,
 birth_date DATE Not Null,
 password varchar(255) Not Null,
 create_date DATETIME Not Null,
 update_date DATETIME Not Null);
 
INSERT INTO user(login_id,name,birth_date,password, create_date,update_date)
VALUES('admin','管理者','2020-01-01','pass',NOW(),NOW());

UPDATE user SET name='境井仁',birth_date='2020-07-17',password='pass',update_date=NOW()
WHERE login_id='id0001';

UPDATE user SET name='小川美幸',birth_date='2020-05-06',password='pass',update_date=NOW()
WHERE (login_id='id0002');

DELETE FROM user WHERE login_id="id0004";
